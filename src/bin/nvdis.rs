// Copyright © 2022 Collabora, Ltd.
// SPDX-License-Identifier: MIT

use nv_shader_tools::nvdis::*;

use clap::Parser;
use std::fs;
use std::io;
use std::io::Read;

/// A nice wrapper around the NVIDIA CUDA disassembler
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Shader model
    #[arg(long)]
    sm: u8,

    /// File name or '-' for stdin
    filename: String,
}

fn main() -> io::Result<()> {
    let args = Args::parse();

    let mut input = Vec::new();
    if args.filename == "-" {
        io::stdin().read_to_end(&mut input)?;
    } else {
        fs::File::open(args.filename.as_str())?.read_to_end(&mut input)?;
    }

    let input = String::from_utf8(input).expect("Invalid UTF-8 input dat");

    let mut bin = Vec::new();
    for w in input.split(char::is_whitespace) {
        if !w.is_empty() {
            let dw = u32::from_str_radix(w, 16).expect("Invaid hex value");
            bin.extend_from_slice(&dw.to_le_bytes()[..]);
        }
    }

    let asm = nvdis(&bin, args.sm)?;
    print!("{asm}");

    Ok(())
}
