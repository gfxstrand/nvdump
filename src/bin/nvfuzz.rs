// Copyright © 2022 Collabora, Ltd.
// SPDX-License-Identifier: MIT

use nv_shader_tools::bitview::*;
use nv_shader_tools::nvdis::*;

use rayon::iter::*;
use std::ops::Range;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let sm: u8 = {
        let sm_str = &args[1];
        assert!(sm_str.starts_with("SM"));
        sm_str[2..].parse().unwrap()
    };
    let range: Vec<&str> = args[2].split("..").collect();
    let range: Range<usize> = Range {
        start: range[0].parse().unwrap(),
        end: range[1].parse().unwrap(),
    };

    let dw_count = if sm >= 70 {
        4
    } else if sm >= 50 {
        8
    } else {
        panic!("Unknown shader model");
    };

    let mut instr = Vec::new();
    for i in 0..dw_count {
        instr.push(u32::from_str_radix(&args[3 + i], 16).unwrap());
    }

    let res: Vec<_> = (0..(1_u64 << range.len()))
        .into_par_iter()
        .map(|bits| {
            let mut bin = Vec::new();
            for dw in &instr {
                bin.extend_from_slice(&dw.to_le_bytes()[..]);
            }
            BitMutView::new(&mut bin[..]).set_field(range.clone(), bits);
            (bits, nvdis_instrs(&bin, sm))
        })
        .collect();

    for (bits, r) in res {
        if let Ok(instrs) = r {
            let instr = if sm >= 70 {
                assert!(instrs.len() == 1);
                &instrs[0]
            } else {
                assert!(range.start >= 64);
                let i = (range.start - 64) / 64;
                &instrs[i]
            };
            println!("With {bits:#x}:    {}", instr.asm);
        }
    }
}
