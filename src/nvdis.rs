// Copyright © 2024 Collabora, Ltd.
// SPDX-License-Identifier: MIT

use std::fmt;
use std::fmt::Write as FmtWrite;
use std::fs;
use std::io;
use std::io::{BufRead, BufReader, Write as IoWrite};
use std::path::PathBuf;
use std::process::Command;

use once_cell::sync::{Lazy, OnceCell};
use regex::Regex;
use tempfile::tempdir;

use crate::bitview::*;

pub struct InstrDeps {
    pub delay: u8,
    pub yld: bool,
    pub wr_bar: Option<u8>,
    pub rd_bar: Option<u8>,
    pub wt_bar_mask: u8,
    pub reuse_mask: u8,
}

impl fmt::Display for InstrDeps {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "delay={}", self.delay)?;
        if self.wt_bar_mask != 0 {
            write!(f, " wt={:06b}", self.wt_bar_mask)?;
        }
        if let Some(rd_bar) = self.rd_bar {
            write!(f, " rd:{rd_bar}")?;
        }
        if let Some(wr_bar) = self.wr_bar {
            write!(f, " wr:{wr_bar}")?;
        }
        if self.reuse_mask != 0 {
            write!(f, " reuse={:06b}", self.reuse_mask)?;
        }
        if self.yld {
            write!(f, " yld")?;
        }
        Ok(())
    }
}

pub struct Instr {
    pub offset: Option<u32>,
    pub asm: String,
    pub deps: InstrDeps,
}

static INST_RE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(concat!(
        r"\s*/\*\s*(?<off>[0-9a-f]*)\s*\*/",
        r"\s*(?<asm>[^;]*)\s*;",
        r"\s*/\*\s*0x(?<hex>[0-9a-f]*)\s*\*/",
    ))
    .unwrap()
});

fn parse_line(line: String) -> io::Result<Option<(usize, String, u64)>> {
    let Some(cap) = INST_RE.captures(&line) else {
        return Ok(None);
    };

    let off = match usize::from_str_radix(&cap["off"], 16) {
        Ok(off) => off,
        Err(e) => {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Failed to parse offset from nvdisasm: {e}"),
            ));
        }
    };

    let asm = cap["asm"].to_lowercase();

    let bits = match u64::from_str_radix(&cap["hex"], 16) {
        Ok(bits) => bits,
        Err(e) => {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Failed to parse instruction hex from nvdisasm: {e}"),
            ));
        }
    };

    Ok(Some((off, asm, bits)))
}

fn parse_instrs_sm50(bin: &[u8], asm: &[u8]) -> io::Result<Vec<Instr>> {
    let mut instrs = Vec::new();
    for line in BufReader::new(&asm[..]).lines() {
        let Some((off, asm, bits)) = parse_line(line?)? else {
            continue;
        };

        assert!(off % 8 == 0);
        let qw = off / 8;
        let bundle = qw / 4;

        assert!(qw % 4 != 0);
        let i = (qw % 4) - 1;

        if instrs.len() != bundle * 3 + i {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Instruction bundles got out-of-sync"),
            ));
        }

        let instr_bits: [u8; 8] = bin[off..(off + 8)].try_into().unwrap();

        // Sanity check that we have the right instruction
        if bits != u64::from_le_bytes(instr_bits) {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Instruction bits don't match"),
            ));
        }

        let deps = &bin[(bundle * 32)..(bundle * 32 + 8)];
        let bv = BitView::new(deps);
        let bv = bv.subset((i * 21)..((i + 1) * 21));
        let delay = bv.get_bit_range_u64(0..4);
        let yld = bv.get_bit(4);
        let wr_bar = bv.get_bit_range_u64(5..8);
        let rd_bar = bv.get_bit_range_u64(8..11);
        let wt_bar_mask = bv.get_bit_range_u64(11..17);
        let reuse_mask = bv.get_bit_range_u64(17..21);
        let deps = InstrDeps {
            delay: delay.try_into().unwrap(),
            yld,
            wr_bar: if wr_bar == 7 {
                None
            } else {
                Some(wr_bar.try_into().unwrap())
            },
            rd_bar: if rd_bar == 7 {
                None
            } else {
                Some(rd_bar.try_into().unwrap())
            },
            wt_bar_mask: wt_bar_mask.try_into().unwrap(),
            reuse_mask: reuse_mask.try_into().unwrap(),
        };

        instrs.push(Instr {
            offset: if i == 0 { Some(off.try_into().unwrap()) } else { None },
            asm,
            deps,
        });
    }
    Ok(instrs)
}

fn parse_instrs_sm70(bin: &[u8], asm: &[u8]) -> io::Result<Vec<Instr>> {
    let mut instrs = Vec::new();
    for line in BufReader::new(&asm[..]).lines() {
        let Some((off, asm, bits)) = parse_line(line?)? else {
            continue;
        };

        assert!(off % 16 == 0);
        let i = off / 16;

        if instrs.len() != i {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Instruction parsing got out-of-sync"),
            ));
        }

        let instr_bits: [u8; 16] = bin[off..(off + 16)].try_into().unwrap();

        // Sanity check that we have the right instruction
        let first_qw = u64::from_le_bytes(instr_bits[0..8].try_into().unwrap());
        if bits != first_qw {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Instruction bits don't match"),
            ));
        }

        let bv = BitView::new(&instr_bits[..]);
        let delay = bv.get_bit_range_u64(105..109);
        let yld = bv.get_bit(109);
        let wr_bar = bv.get_bit_range_u64(110..113);
        let rd_bar = bv.get_bit_range_u64(113..116);
        let wt_bar_mask = bv.get_bit_range_u64(116..122);
        let reuse_mask = bv.get_bit_range_u64(122..126);
        let deps = InstrDeps {
            delay: delay.try_into().unwrap(),
            yld,
            wr_bar: if wr_bar == 7 {
                None
            } else {
                Some(delay.try_into().unwrap())
            },
            rd_bar: if rd_bar == 7 {
                None
            } else {
                Some(delay.try_into().unwrap())
            },
            wt_bar_mask: wt_bar_mask.try_into().unwrap(),
            reuse_mask: reuse_mask.try_into().unwrap(),
        };

        instrs.push(Instr {
            offset: Some(off.try_into().unwrap()),
            asm,
            deps,
        });
    }
    Ok(instrs)
}

fn get_nvdisasm() -> io::Result<&'static PathBuf> {
    static NVDISASM: OnceCell<PathBuf> = OnceCell::new();

    NVDISASM.get_or_try_init(|| {
        let paths = fs::read_dir("/usr/local")?;

        for path in paths {
            let mut path = path?.path();

            let Some(fname) = path.file_name() else {
                continue;
            };

            let Some(fname) = fname.to_str() else {
                continue;
            };

            if !fname.starts_with("cuda-") {
                continue;
            }

            path.push("bin");
            path.push("nvdisasm");
            if path.exists() {
                return Ok(path);
            }
        }

        if let Some(paths) = std::env::var_os("PATH") {
            for dir in std::env::split_paths(&paths) {
                let full_path = dir.join("nvdisasm");

                if full_path.is_file() {
                    return Ok(full_path);
                }
            }
        }

        Err(io::Error::new(
            io::ErrorKind::NotFound,
            "Failed to find nvdisasm",
        ))
    })
}

pub fn nvdis_raw(bin: &[u8], sm: u8) -> io::Result<Vec<u8>> {
    let nvdisasm = get_nvdisasm()?;

    let tmp_dir = tempdir()?;
    let path = tmp_dir.path().join("shader.bin");
    {
        let mut file = fs::File::create(&path)?;
        file.write_all(bin)?;
        file.flush()?;
    }

    let out = Command::new(nvdisasm)
        .arg("-hex")
        .arg("-b")
        .arg(format!("SM{sm}"))
        .arg(path)
        .output()?;

    tmp_dir.close()?;

    if out.status.success() {
        Ok(out.stdout)
    } else {
        Err(io::Error::new(
            io::ErrorKind::InvalidData,
            String::from_utf8_lossy(&out.stderr).to_owned(),
        ))
    }
}

pub fn nvdis_instrs(bin: &[u8], sm: u8) -> io::Result<Vec<Instr>> {
    let raw = nvdis_raw(bin, sm)?;
    if sm >= 70 {
        parse_instrs_sm70(bin, &raw)
    } else if sm >= 50 {
        parse_instrs_sm50(bin, &raw)
    } else {
        panic!("Unknown shader model {sm}");
    }
}

pub fn nvdis(bin: &[u8], sm: u8) -> io::Result<String> {
    let instrs = nvdis_instrs(bin, sm)?;
    let width = instrs.iter().map(|i| i.asm.len()).fold(0, std::cmp::max);

    let mut s = String::new();
    for instr in nvdis_instrs(bin, sm)? {
        if let Some(off) = instr.offset {
            write!(s, "{off:#08x}:  ").unwrap();
        } else {
            write!(s, "           ").unwrap();
        }
        writeln!(s, "{:<width$} // {}", instr.asm, instr.deps).unwrap();
    }
    Ok(s)
}
